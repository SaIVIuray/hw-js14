const images = document.querySelectorAll(".image-to-show");
      const stopButton = document.getElementById("stopButton");
      const resumeButton = document.getElementById("resumeButton");
      const timer = document.getElementById("timer");
      const changeThemeButton = document.getElementById("changeThemeButton");
      let currentIndex = 0;
      let isPlaying = true;
      let intervalId;
      let showNextImageTimeout;

      
      function toggleTheme() {
        const body = document.body;
        const currentTheme = localStorage.getItem("theme");

        if (currentTheme === "dark") {
          body.style.backgroundColor = "white";
          localStorage.setItem("theme", "light");
          stopButton.classList.remove("btn-dark");
          stopButton.classList.add("btn-light");
          resumeButton.classList.remove("btn-dark");
          resumeButton.classList.add("btn-light");
          images.forEach((img) => {
            img.classList.remove("image-border-dark");
            img.classList.add("image-border-light");
          });
        } else {
          body.style.backgroundColor = "rgb(63, 63, 63)";
          localStorage.setItem("theme", "dark");
          stopButton.classList.remove("btn-light");
          stopButton.classList.add("btn-dark");
          resumeButton.classList.remove("btn-light");
          resumeButton.classList.add("btn-dark");
          images.forEach((img) => {
            img.classList.remove("image-border-light");
            img.classList.add("image-border-dark");
          });
        }
      }

      function setTheme() {
        const currentTheme = localStorage.getItem("theme");
        const body = document.body;
        const timer = document.getElementById("timer-dark");
      
        if (currentTheme === "light") {
          body.style.backgroundColor = "white";
          stopButton.classList.remove("btn-dark");
          stopButton.classList.add("btn-light");
          resumeButton.classList.remove("btn-dark");
          resumeButton.classList.add("btn-light");
          images.forEach((img) => {
            img.classList.remove("image-border-dark");
            img.classList.add("image-border-light");
          });
          timer.classList.remove("timer-dark");
        } else {
          body.style.backgroundColor = "rgb(63, 63, 63)";
          stopButton.classList.remove("btn-light");
          stopButton.classList.add("btn-dark");
          resumeButton.classList.remove("btn-light");
          resumeButton.classList.add("btn-dark");
          images.forEach((img) => {
            img.classList.remove("image-border-light");
            img.classList.add("image-border-dark");
          });
          timer.classList.add("timer-dark");
        }
      }
      
      if (localStorage.getItem("theme") === null) {
        localStorage.setItem("theme", "dark");
      }
      
      setTheme();

      function toggleTheme() {
        const body = document.body;
        const currentTheme = localStorage.getItem("theme");
        const timer = document.getElementById("timer-dark");
      
        if (currentTheme === "dark") {
          body.style.backgroundColor = "white";
          localStorage.setItem("theme", "light");
          stopButton.classList.remove("btn-dark");
          stopButton.classList.add("btn-light");
          resumeButton.classList.remove("btn-dark");
          resumeButton.classList.add("btn-light");
          images.forEach((img) => {
            img.classList.remove("image-border-dark");
            img.classList.add("image-border-light");
          });
          timer.classList.remove("timer-dark");
        } else {
          body.style.backgroundColor = "rgb(63, 63, 63)";
          localStorage.setItem("theme", "dark");
          stopButton.classList.remove("btn-light");
          stopButton.classList.add("btn-dark");
          resumeButton.classList.remove("btn-light");
          resumeButton.classList.add("btn-dark");
          images.forEach((img) => {
            img.classList.remove("image-border-light");
            img.classList.add("image-border-dark");
          });
          timer.classList.add("timer-dark");
        }
      }
      
      if (localStorage.getItem("theme") === null) {
        localStorage.setItem("theme", "dark");
      }

      setTheme();

      changeThemeButton.addEventListener("click", toggleTheme);

      function showImage(index) {
        const images = document.querySelectorAll(".image-to-show");
        images.forEach((img, i) => {
          if (i === index) {
            img.style.display = "block";
            img.style.opacity = "1";
          } else {
            img.style.display = "none";
          }
        });
      }

      function nextImage() {
        currentIndex = (currentIndex + 1) % images.length;
        showImage(currentIndex);
        startTimer(3);
      }

      let remainingSeconds ="3";
      function startTimer(seconds) {
        clearInterval(intervalId);
        remainingSeconds = seconds; 
        const timer = document.getElementById("timer-dark");
        timer.innerText = `До наступного зображення: ${remainingSeconds} с`;
      
        intervalId = setInterval(() => {
          remainingSeconds--;
          timer.innerText = `До наступного зображення: ${remainingSeconds} с`;
      
          if (remainingSeconds === 0) {
            clearInterval(intervalId);
            fadeOutCurrentImage();
          }
        }, 1000);
      }
      
      function resumeSlideshow() {
        if (isPlaying) {
          startTimer(remainingSeconds);
        }
      }

      function fadeOutCurrentImage() {
        const currentImage = images[currentIndex];
        let opacity = 1;

        const fadeOutInterval = setInterval(() => {
          opacity -= 0.1;
          currentImage.style.opacity = opacity.toFixed(1);

          if (opacity <= 0) {
            clearInterval(fadeOutInterval);
            nextImage();
          }
        }, 50);
      }

      function resumeSlideshow() {
        if (isPlaying) {
          startTimer(3);
        }
      }

      showImage(0);
      startTimer(3);

      stopButton.addEventListener("click", () => {
        isPlaying = false;
        clearInterval(intervalId);
      });

      resumeButton.addEventListener("click", () => {
        isPlaying = true;
        resumeSlideshow();
      });